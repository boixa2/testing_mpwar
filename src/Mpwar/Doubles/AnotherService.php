<?php

namespace Mpwar\Doubles;

interface AnotherService
{
    public function getNumberType($number);
}